let factorialNumber = +prompt('Enter number ', '');

while(isNaN(factorialNumber) || factorialNumber === 0 || factorialNumber !== parseInt(factorialNumber)) {
    factorialNumber = +prompt('Enter correct number','');
}

function factorial(factorialNumber){
    if(factorialNumber !=1){
        return factorialNumber * factorial(factorialNumber-1);
    } else {
        return 1;
    }
}

alert(`Your factorial is : ${factorial(factorialNumber)}`);